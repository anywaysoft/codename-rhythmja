﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class stackFSM : ScriptableObject {

    
    public delegate void stateFunc();
    private List<stateFunc> stack;

    // Use this for initialization
	void Start () {
        stack = new List<stateFunc>();
	}

    public stateFunc popState() {
        stateFunc state = stack[stack.Count - 1];
        stack.RemoveAt(stack.Count - 1);
        return state;
    }

    public void pushState(stateFunc newState) {
        if (getCurrentState() != newState) {
            stack.Add(newState);
        }
    }

    stateFunc getCurrentState() {
        if (stack.Count != 0) {
            return stack[stack.Count - 1];
        }
        return null;
    }

	// Update is called once per frame
	void Update () {
        stateFunc state = getCurrentState();
        if (state != null) {
            state();
        }
	}
}
