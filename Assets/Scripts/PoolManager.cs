﻿using UnityEngine;
using System.Collections;

public class PoolManager : MonoBehaviour {

	PoolManager()
	{

	}
	public int PoolIndex;
	public ObjectPool Pool;

	void OnDisable()
	{

		if (Pool != null) {
						Pool.getElementArray () [PoolIndex].isAvailable = false;
						Pool.getElementArray () [PoolIndex].setTimer ((float)0.01);
				}
	}

}
