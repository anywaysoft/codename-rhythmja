﻿using UnityEngine;
using System.Collections;

public class TestMovementController : MonoBehaviour {

    Rigidbody2D body;
	// Use this for initialization
	void Start () {
        body = gameObject.GetComponent<Rigidbody2D>();
	}
	
    void processMovement()
    {
        if(Input.GetKey(KeyCode.D))
        {
            body.AddForce(new Vector2(20, 0));
        }
        if (Input.GetKey(KeyCode.A))
        {
            body.AddForce(new Vector2(-20, 0));
        }
    }
	// Update is called once per frame
	void Update () {
        processMovement();
	}
}
