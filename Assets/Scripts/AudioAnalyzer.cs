﻿using UnityEngine;
using System.Collections;

public class AudioAnalyzer : MonoBehaviour {

    AudioListener listener;

	// Use this for initialization
	void Start () {
        listener = GetComponent<AudioListener>();
        if (listener == null) {
            Debug.Log("There is no AudioListener attached");
            GameObject.Destroy(gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
        
	}
}
