﻿using UnityEngine;
using System.Collections;

public class scrollingBack : MonoBehaviour {

    public float speed;
    public GameObject target;
    Rigidbody2D body;
	// Use this for initialization
	void Start () {
        body = target.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
            if(body.velocity.x > 0)
            {
                gameObject.GetComponent<Rigidbody>().AddTorque(new Vector3(0, speed, 0));
            }
        else
        {
            if (body.velocity.x < 0)
            {
                gameObject.GetComponent<Rigidbody>().AddTorque(new Vector3(0, -speed, 0));
            }
        }
	
	}
}
