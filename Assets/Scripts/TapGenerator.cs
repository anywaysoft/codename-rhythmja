﻿using UnityEngine;
using System.Collections;

public class TapGenerator : MonoBehaviour {

    public GameObject tap;
    static ObjectPool tapPool;
    GameObject successEffect, failEffect;
    ObjectPool successEffectPool, failEffectPool;
    int currentTapNumber;
    int successfulTapNumber;
    int activeTapNumber;
    bool sequenceInProcess;
    public float spawnRadius;

    public bool getSequenceStatus()
    {
        return sequenceInProcess;
    }
    public void setSuccesfulTapNumber(int iNumber)
    {
        successfulTapNumber = iNumber;
    }
    public int getSuccesfulTapNumber()
    {
        return successfulTapNumber;
    }
    public void setActiveTapNumber(int iNumber)
    {
        activeTapNumber = iNumber;
    }
    public int getActiveTapNumber()
    {
        return activeTapNumber;
    }
	// Use this for initialization
	void Start () {
        if(tapPool == null)
        {
            tapPool = new ObjectPool(15, tap);
        }
        sequenceInProcess = false;
        successEffect = Resources.Load("Prefabs/successEffect") as GameObject;
        successEffectPool = new ObjectPool(3, successEffect);
        failEffect = Resources.Load("Prefabs/failEffect") as GameObject;
        failEffectPool = new ObjectPool(3, failEffect);
	}
	void processSpawning()
    {
        
        
            GameObject clone;
            Vector3 offset = new Vector3 (Random.Range(0, spawnRadius), Random.Range(0, spawnRadius), 0);
            clone = tapPool.getNextObjectFromPool(gameObject.transform.position - offset, gameObject.transform.rotation);
            clone.transform.FindChild("tapCircle").gameObject.transform.localScale = tap.transform.FindChild("tapCircle").gameObject.transform.localScale ;
            clone.GetComponent<SpriteRenderer>().enabled = true;
            clone.transform.FindChild("tapCircle").gameObject.GetComponent<SpriteRenderer>().enabled = true;
            
        
    }

    void OnEnemy()
    {
        if (!sequenceInProcess)
        {
            sequenceInProcess = true;
            currentTapNumber = Random.Range(1, 4);
            activeTapNumber = currentTapNumber;
            for (int i = 0; i < currentTapNumber; i++)
            {
                processSpawning();
            }
        }
           
    }

    void isSequenceInProcess()
    {
        if (sequenceInProcess)
        {
            if (activeTapNumber == 0)
            {
                sequenceInProcess = false;
                if(currentTapNumber == successfulTapNumber)
                {
                    successEffectPool.getNextObjectFromPool(gameObject.transform.position, gameObject.transform.rotation);
                }
                else
                {
                    failEffectPool.getNextObjectFromPool(gameObject.transform.position, gameObject.transform.rotation);
                }
                activeTapNumber = 0;
                successfulTapNumber = 0;
            }
        }
    }

	// Update is called once per frame
	void Update () {
        isSequenceInProcess();
	
	}

    
}
