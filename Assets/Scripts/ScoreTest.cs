﻿using UnityEngine;
using System.Collections;

public class ScoreTest : MonoBehaviour {

    GameObject effect;
	// Use this for initialization
	void Start () {
        effect = Resources.Load("Prefabs/scoreEffect") as GameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        Instantiate(effect);
    }
}
