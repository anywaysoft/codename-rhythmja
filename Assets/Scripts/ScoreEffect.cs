﻿using UnityEngine;
using System.Collections;

public class ScoreEffect : MonoBehaviour
{
    bool reachedMax;
    bool movementEnded;
    public Vector2 movementVector;
    float showingTime;
    public float showingTimeout;
    public float startSize, maxSize;
    public float resizeStep;
    public float resizeTimeout;
    float resizeTime;
    Vector2 MovementForce;
    Rigidbody2D body;
    static GameObject startObject;



    void processShowing()
    {
        showingTime += Time.deltaTime;
        if(showingTime >= showingTimeout)
        {
            showingTime = 0;
            OnDeactivate();
        }
    }
    void processResizing()
    {
        resizeTime += Time.deltaTime;
        if (resizeTime >= resizeTimeout)
        {
            if (!reachedMax)
            {
                gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + resizeStep,
                                                                gameObject.transform.localScale.y + resizeStep,
                                                                gameObject.transform.localScale.z);

            }
            else
            {
                if (gameObject.transform.localScale.x >= 0 && gameObject.transform.localScale.y >= 0)
                {
                    gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x - resizeStep,
                                                                    gameObject.transform.localScale.y - resizeStep,
                                                                    gameObject.transform.localScale.z);
                }
                else
                {
                   
                }
            }
        }
        if(gameObject.transform.localScale.x >= maxSize || gameObject.transform.localScale.y >= maxSize)
        {
            reachedMax = true;
            

        }
    }


    void processMovement()
    {
        MovementForce += movementVector;
    }

    void addMovement()
    {
        body.AddForce(MovementForce);
        MovementForce = new Vector2(0, 0);
    }
    // Use this for initialization

    void OnDeactivate()
    {
        reachedMax = false;
        gameObject.transform.localScale = new Vector3(startSize,
                                                        startSize,
                                                        gameObject.transform.localScale.z);
        gameObject.SetActive(false);
    }
    void Start()
    {
        gameObject.transform.localScale = new Vector3(startSize,
                                                        startSize,
                                                        gameObject.transform.localScale.z);
        reachedMax = false;
        body = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        processResizing();
        processMovement();
        processShowing();
    }

    void OnEnable()
    {
        reachedMax = false;
    }

    void FixedUpdate()
    {
        addMovement();
    }
}
