using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool: ScriptableObject
{
	// this is object pool with trail renderer fix.



	public void UpdatePool()
	{
		// this is a function to update timers for cooling down the objects(to fix the trail renderer error)
		foreach (ObjectPoolObject c in mObjectStorage) {
			c.updateTimer();
				}
	}


	public ObjectPool(int iPoolSize, GameObject iPooledObject)
	{
		mObjectStorage = new List<ObjectPoolObject>();
		if (iPooledObject.GetComponent<TrailRenderer> () != null) {
						mTrailTime = iPooledObject.GetComponent<TrailRenderer> ().time;
						isThereATrail = true;
						createPoolWithTrail(iPoolSize, iPooledObject);
				} 	
			else {
						createPool(iPoolSize, iPooledObject);
					}
	}

	public GameObject getNextObjectFromPool()
	{
		//only for objects without trail renderer
		for (int i = 0; i < mObjectStorage.Count; i++) {
			if (mObjectStorage [i].getObject ().activeInHierarchy == false) {
					mObjectStorage [i].getObject ().SetActive (true);
					return mObjectStorage [i].getObject ();
				}
				}
		return null;
	}



	public GameObject getNextObjectFromPool(Vector3 pos, Quaternion rot)
	{
		if (isThereATrail) {
			return getObjectWithTrailFromPool(pos, rot);
							}
				else {
			return getObjectFromPool(pos, rot);
		}
		return null;
	}




	public List<ObjectPoolObject> getElementArray()
	{
		return mObjectStorage;
	}

	void createPoolWithTrail(int iPoolSize, GameObject iPooledObject)
	{
		// pool with trail renderer fix
		for (int i = 0; i< iPoolSize; i++) {
			GameObject clone;
			clone = Instantiate (iPooledObject) as GameObject;
			clone.SetActive (false);
			// adding poolmanager behaviour to object with the purpose to control object cool down 
			clone.AddComponent<PoolManager> ();
			clone.GetComponent<PoolManager> ().PoolIndex = i;
			clone.GetComponent<PoolManager> ().Pool = this;
			mObjectStorage.Add (new ObjectPoolObject (clone));
		}
	}

	void createPool(int iPoolSize, GameObject iPooledObject)
	{
		// regular object pool
		for (int i = 0; i< iPoolSize; i++) {
			GameObject clone;
			clone = Instantiate (iPooledObject) as GameObject;
			clone.SetActive (false);
			mObjectStorage.Add (new ObjectPoolObject (clone));
		}
	}

	GameObject getObjectWithTrailFromPool(Vector3 pos, Quaternion rot)
	{
		for (int i = 0; i < mObjectStorage.Count; i++) {
			if (mObjectStorage [i].getObject ().activeInHierarchy == false) {
				mObjectStorage [i].updateStatus ();
				if (mObjectStorage [i].isAvailable) {
					if (mObjectStorage [i].getObject ().GetComponent<TrailRenderer> ().time == 0) {
						mObjectStorage [i].getObject ().GetComponent<TrailRenderer> ().time = mTrailTime;
						
					} else {
						mObjectStorage [i].getObject ().transform.position = pos;
						mObjectStorage [i].getObject ().transform.rotation = rot;
						mObjectStorage [i].getObject ().SetActive (true);
						return mObjectStorage [i].getObject ();
					}
				}
				
			}
		}
		return null;
	}

	GameObject getObjectFromPool (Vector3 pos, Quaternion rot)
	{
		for (int i = 0; i < mObjectStorage.Count; i++) {
			mObjectStorage[i].updateStatus();
			if (mObjectStorage [i].isAvailable) {
				if (mObjectStorage [i].getObject ().activeInHierarchy == false) {
					if (mObjectStorage [i].getObject ().activeInHierarchy == false) {
						mObjectStorage [i].getObject ().transform.position = pos;
						mObjectStorage [i].getObject ().transform.rotation = rot;
						mObjectStorage [i].getObject ().SetActive (true);
						return mObjectStorage [i].getObject ();
					}
				}
			}
		}
		return null;

	}
	



	

	List<ObjectPoolObject> mObjectStorage;
	float mTrailTime;
	bool isThereATrail;
}

