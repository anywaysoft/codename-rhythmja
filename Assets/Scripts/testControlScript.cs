﻿using UnityEngine;
using System.Collections;

public class testControlScript : MonoBehaviour {

    Vector2 force;
    Vector2 dforce;
    float maxforce;
    Rigidbody2D body;

	// Use this for initialization
	void Start () {
        force = new Vector2(0, 0);
        dforce = new Vector2(0.1f, 0);
        maxforce = 10f;
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.W))
        {
            body.velocity += new Vector2(0, 10);
        }
            if ((force + dforce).magnitude < maxforce)
            {
                force += dforce;
            }
    }
    void FixedUpdate() {
        body.AddForce(force, ForceMode2D.Impulse);
    }
}
