﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

    static TapGenerator tapGenerator;
    Animator hitEffect;
    bool sequenceInProcess;
    bool sequenceWasStarted;
    bool playerHasCollided;
	// Use this for initialization
	void Start () {
        sequenceWasStarted = false;
	    if(tapGenerator == null)
        {
           tapGenerator = GameObject.FindGameObjectWithTag("tapGenerator").GetComponent<TapGenerator>();
        }
        playerHasCollided = false;
	}
	
	// Update is called once per frame
	void Update () {
        checkSequence();
	}


    void checkSequence()
    {
        if (sequenceWasStarted)
        {
            if (tapGenerator.getSequenceStatus() == false)
            {
                if (playerHasCollided)
                {
                    OnDeactivation();
                }
            }
        }
    }
    void OnDeactivation()
    {
        sequenceWasStarted = false;
        playerHasCollided = false;
        Deactivate();
    }

    void Deactivate()
    {

        gameObject.SetActive(false);
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (!sequenceWasStarted)
        {
            if (coll.gameObject.tag == "Player")
            {
                sequenceWasStarted = true;
                tapGenerator.SendMessage("OnEnemy");
            }
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {

        if (coll.gameObject.tag == "Player")
        {
            playerHasCollided = true;
        }
    }
}
