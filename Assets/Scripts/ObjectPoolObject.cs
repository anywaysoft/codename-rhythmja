﻿using UnityEngine;
using System.Collections;

public class ObjectPoolObject : ScriptableObject {
	//to allow timeout for objects this is a storage class
	const float OBJECT_TIME_OUT = (float)0.5;
	


	public ObjectPoolObject()
	{
	}
	public ObjectPoolObject(GameObject iGameObject)
	{
		mGameObject = iGameObject;
		mTimeout = 0;
		isAvailable = true;
	}
    



	public void updateTimer()
	{
		if (mTimeout != 0)  {

						if (mTimeout > 0) {
								mTimeout += Time.deltaTime;
						}
						if (mTimeout >= OBJECT_TIME_OUT) {
								mTimeout = 0;


						}
				}
		}
	public float getTimer()
	{
		return mTimeout;
	}
	public void setTimer(float iTime)
	{
		mTimeout = iTime;
	}

	public GameObject getObject()
	{
		return mGameObject;
	}

	public void updateStatus()
	{
		if (mTimeout == 0) {
						isAvailable = true;
				} else 
						isAvailable = false;
	}
	
	GameObject mGameObject;
	float mTimeout;
	public bool isAvailable;
}
