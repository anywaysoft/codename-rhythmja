﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class groundSpawner : MonoBehaviour {

    public int elementsCount;
    public Vector2 startPoint;
    public float segmentHeightAmplitude;
    public float width;
    public Shader shader;

    private const int pplCollider = 1001;
    private const int pplRenderer = 101;
    private Mesh mesh;
    private Material meshMat;
    private EdgeCollider2D edgeCollider;
    private float height;
    private Vector2 [] curveCollider;
    private Vector2 [] curveRenderer;
    

    void Start () {
        height = startPoint.y;
        edgeCollider = GetComponent<EdgeCollider2D>();
        if (edgeCollider == null) {
            Debug.Log("There is no EdgeCollider2D attached");
            Object.Destroy(gameObject);
        }
        mesh = new Mesh();
        meshMat = new Material(shader);
        curveCollider = new Vector2[elementsCount * pplCollider];
        curveRenderer = new Vector2[elementsCount * pplRenderer];
        calcPoints();
        fillCollider();
        fillRenderer();

        Debug.DrawLine(startPoint + new Vector2(0, segmentHeightAmplitude), curveCollider[curveCollider.GetLength(0) - 1] + new Vector2(0, segmentHeightAmplitude), Color.white, 10000f);
        Debug.DrawLine(startPoint + new Vector2(0, -segmentHeightAmplitude), curveCollider[curveCollider.GetLength(0) - 1] + new Vector2(0, -segmentHeightAmplitude), Color.white, 10000f);
	}

    void fillCollider() {
        edgeCollider.points = curveCollider;
    }

    void fillRenderer() {
        Vector3 [] meshPoints = new Vector3[2 * curveRenderer.GetLength(0)];
        int [] meshTriangls = new int[6 * (curveRenderer.GetLength(0) - 1)];
        meshPoints[0] = curveRenderer[0];
        meshPoints[1] = new Vector2(curveRenderer[0].x, height - width);
        for (int pIndex = 1; pIndex < curveRenderer.GetLength(0); ++pIndex) {
            meshPoints[pIndex * 2] = curveRenderer[pIndex];
            meshPoints[pIndex * 2 + 1] = new Vector2(curveRenderer[pIndex].x, height - width);
            meshTriangls[(pIndex - 1) * 6] = 2 * pIndex - 2;
            meshTriangls[(pIndex - 1) * 6 + 1] = 2 * pIndex - 1;
            meshTriangls[(pIndex - 1) * 6 + 2] = 2 * pIndex;
            meshTriangls[(pIndex - 1) * 6 + 3] = 2 * pIndex - 1;
            meshTriangls[(pIndex - 1) * 6 + 4] = 2 * pIndex + 1;
            meshTriangls[(pIndex - 1) * 6 + 5] = 2 * pIndex;
        }
        mesh.vertices = meshPoints;
        mesh.triangles = meshTriangls;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
    }

    void calcPoints()
    {
        Vector2 curPointStart = startPoint;
        Vector2 prevPointMiddle = new Vector2(curPointStart.x + Random.Range(20f, 31f), height + Random.Range(-segmentHeightAmplitude, segmentHeightAmplitude + 1));
        
        for (int i = 0; i < elementsCount; ++i)
        {
            float k = (curPointStart.x - prevPointMiddle.x) / (curPointStart.y - prevPointMiddle.y);
            float curPMy = height + Mathf.Sign(k) * Random.Range(segmentHeightAmplitude, segmentHeightAmplitude + 1);
            float curPMx = k * (curPMy - curPointStart.y) + curPointStart.x;
            Vector2 curPointMiddle = new Vector2(curPMx, curPMy);
            float segWidth = (curPMx - curPointStart.x) * 2;
            segWidth += Random.Range(-segWidth / 8, segWidth / 6 + 1);
            Vector2 curPointEnd = curPointStart + new Vector2(segWidth, height);

            Vector2 [] newCurvePart = Curves.quadraticBezierCurve(curPointStart, curPointMiddle, curPointEnd, pplCollider);
            for (int pIndex = 0; pIndex < pplCollider; ++pIndex) {
                curveCollider[pIndex + pplCollider * i] = newCurvePart[pIndex];
            }
            newCurvePart = Curves.quadraticBezierCurve(curPointStart, curPointMiddle, curPointEnd, pplRenderer);
            for (int pIndex = 0; pIndex < pplRenderer; ++pIndex)
            {
                curveRenderer[pIndex + pplRenderer * i] = newCurvePart[pIndex];
            }

            Debug.DrawLine(curPointStart, curPointMiddle, Color.red, 10000f);
            Debug.DrawLine(curPointMiddle, curPointEnd, Color.red, 10000f);
            Debug.DrawLine(curPointStart + new Vector2(0, -segmentHeightAmplitude), curPointStart + new Vector2(0, segmentHeightAmplitude), Color.blue, 10000f);
            Debug.DrawLine(curPointEnd + new Vector2(0, -segmentHeightAmplitude), curPointEnd + new Vector2(0, segmentHeightAmplitude), Color.blue, 10000f);


            prevPointMiddle = curPointMiddle;
            curPointStart = curPointEnd;
        }
    }

    void Update() {
        Graphics.DrawMesh(mesh, transform.localToWorldMatrix, meshMat, 0);
    }
}
