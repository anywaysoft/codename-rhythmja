﻿using UnityEngine;
using System.Collections;

public class testEnemySpawner : MonoBehaviour {

    public GameObject enemy;
    ObjectPool enemyPool;
    public float spawnTimeout;
    float spawnTime;
	// Use this for initialization
	void Start () {
        enemyPool = new ObjectPool(5, enemy);
	    
	}
	
	// Update is called once per frame
	void Update () {
        spawnTime += Time.deltaTime;
        if(spawnTime >= spawnTimeout)
        {
            GameObject clone;
            clone = enemyPool.getNextObjectFromPool(gameObject.transform.position, enemy.transform.rotation);
            spawnTime = 0;
        }
	}
}
