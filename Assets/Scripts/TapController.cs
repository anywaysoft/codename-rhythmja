﻿using UnityEngine;
using System.Collections;

public class TapController : MonoBehaviour {

    bool wasTouched;
    static GameObject effect;
    static ObjectPool effectPool;
    GameObject circle;
    static GameObject tapCamera;
    static TapGenerator tapGenerator;
    float circleResizingTime;
    public float circleResizingTimeout;
    public float circleResizingStep;
    public float tapRadius;
    SpriteRenderer circleRenderer;

	// Use this for initialization
	void Start () {
        wasTouched = false;
        circleRenderer = gameObject.transform.FindChild("tapCircle").gameObject.GetComponent<SpriteRenderer>();
        if (effect == null)
        {
            effect = Resources.Load("Prefabs/scoreEffect") as GameObject;
        }
        if(effectPool == null)
        {
            effectPool = new ObjectPool(50, effect);
        }
        circle = gameObject.transform.FindChild("tapCircle").gameObject;
        if (tapCamera == null)
        {
            tapCamera = GameObject.FindGameObjectWithTag("tapCamera");
        }
        if(tapGenerator == null)
        {
            tapGenerator = GameObject.FindGameObjectWithTag("tapGenerator").GetComponent<TapGenerator>();
        }
	}
	
    void OnDeactivation()
    {
        wasTouched = false;
        gameObject.SetActive(false);
        int activeNumber = tapGenerator.getActiveTapNumber() - 1;
        tapGenerator.setActiveTapNumber(activeNumber);
    }

    void OnTap()
    {

        if (!wasTouched)
        {
            float difference;
            Vector3 MousePosition = Input.mousePosition;
            MousePosition = new Vector3(MousePosition.x, MousePosition.y, 0);
            MousePosition = tapCamera.GetComponent<Camera>().ScreenToWorldPoint(MousePosition);
            //Debug.DrawLine((gameObject.transform.position), MousePosition, Color.white, 1000f);
            difference = ((gameObject.transform.position) - MousePosition).magnitude;
            //Debug.Log(difference);
            //  Debug.Log((tapRadius - difference)*100);
            gameObject.GetComponent<AudioSource>().Play();
            circleRenderer.enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            GameObject clone;
            clone = effectPool.getNextObjectFromPool(gameObject.transform.position, gameObject.transform.rotation);
            clone.transform.localScale = effect.transform.localScale;
            int succesfulNumber = tapGenerator.getSuccesfulTapNumber() + 1;
            tapGenerator.setSuccesfulTapNumber(succesfulNumber);
            OnDeactivation();
        }
    }

    void processTapCircle()
    {
        circleResizingTime += Time.deltaTime;
        if (circle.transform.localScale.x - circleResizingStep > 0 && circle.transform.localScale.y - circleResizingStep > 0)
        {
            if (circleResizingTime >= circleResizingTimeout)
            {
                circleResizingTime = 0;
                circle.transform.localScale = new Vector3(circle.transform.localScale.x - circleResizingStep, 
                    circle.transform.localScale.y - circleResizingStep, circle.transform.localScale.z);
            }
        }
        else
        {
            OnDeactivation();



        }
    }
	// Update is called once per frame
	void Update () {
        processTapCircle();
	}

    void OnMouseDown()
    {
        OnTap();
    }
}
