﻿using UnityEngine;
using System.Collections;

public class Curves : ScriptableObject {

    public static void drawCurve(Vector2[] curve, Color color, float duration)
    {
        Vector2 prevPoint = curve[0];
        for (int i = 1; i < curve.GetLength(0); ++i)
        {
            Debug.DrawLine(prevPoint, curve[i], color, duration);
            prevPoint = curve[i];
        }
    }


    public static Vector2[] linearBezierCurve(Vector2 p0, Vector2 p1, int ppl)
    {
        Vector2 [] curve = new Vector2 [ppl];
        int i = 0;
        for (float t = 0; t <= 1; t += 1f/(ppl - 1))
        {
            Vector2 point = p0 + t * (p1 - p0);
            curve[i++] = point;
        }
        return curve;
    }

    public static Vector2[] quadraticBezierCurve(Vector2 p0, Vector2 p1, Vector2 p2, int ppl)
    {
        Vector2[] curve = new Vector2[ppl];
        int i = 0;
        for (float t = 0; t <= 1; t += 1f/(ppl - 1))
        {
            Vector2 point = (1f - t) * ((1 - t) * p0 + t * p1) + t * ((1 - t) * p1 + t * p2);
            curve[i++] = point;
        }
        return curve;
    }

    public static void drawQuad(Vector2[] quad, Color color, float duration)
    {
        Debug.DrawLine(quad[0], quad[1], color, duration);
        Debug.DrawLine(quad[1], quad[2], color, duration);
        Debug.DrawLine(quad[2], quad[3], color, duration);
        Debug.DrawLine(quad[3], quad[0], color, duration);
    }

    public static Vector2[] makeQuad(Vector2 startPoint, Vector2 endPoint, float width)
    {
        Vector2[] quad = new Vector2[4];
        width = width / 2;
        Vector2 dir = new Vector2(-(endPoint.y - startPoint.y), endPoint.x - startPoint.x);
        dir.Normalize();
        quad[0] = startPoint + dir * width;
        quad[1] = endPoint + dir * width;
        quad[2] = endPoint - dir * width;
        quad[3] = startPoint - dir * width;
        return quad;
    }
}